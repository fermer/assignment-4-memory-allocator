#include "mem.h"
#include "mem_internals.h"
#include <unistd.h>

#define assert(condition) if (!(condition)) { \
         printf("Test failed!\n");            \
         return false;\
}
void test(bool (*test_func()), char* test_name) {
    if (test_func()) {
        printf("Test \"%s\" passed!\n", test_name);
    }
}
bool test_success_alloc() {
    heap_init(1);
    uint8_t* mem = _malloc(100);
    struct block_header* header = block_get_header(mem);
    assert(header->capacity.bytes == 100);

    _free(mem);
    heap_term();
    return true;
}

bool test_free_one_block() {
    heap_init(1);
    uint8_t* mem = _malloc(100);
    uint8_t* mem1 = _malloc(50);
    uint8_t* mem2 = _malloc(200);
    _free(mem1);

    assert(!block_get_header(mem)->is_free);
    assert(block_get_header(mem1)->is_free);
    assert(!block_get_header(mem2)->is_free);

    _free(mem);
    _free(mem2);
    heap_term();
    return true;
}

bool test_free_some_blocks() {
    heap_init(1);
    uint8_t* mem = _malloc(100);
    block_capacity capacity = block_get_header(mem)->next->capacity;
    uint8_t* mem1 = _malloc(50);
    uint8_t* mem2 = _malloc(200);
    _free(mem2);
    _free(mem1);
    assert(block_get_header(mem1)->capacity.bytes == capacity.bytes);
    _free(mem);
    heap_term();
    return true;
}

bool test_new_region() {
    heap_init(1);
    uint8_t* mem = _malloc(10000);
    struct block_header* header = block_get_header(mem);
    assert(header->capacity.bytes == 10000 && header->next->capacity.bytes > REGION_MIN_SIZE)
    return true;
}

bool test_new_region_other_place() {
    void* odd_alloc = mmap(HEAP_START + REGION_MIN_SIZE + 1, 30, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0 );
    heap_init(1);
    uint8_t* mem = _malloc(10000);
    struct block_header* header = block_get_header(mem);

    assert(header != odd_alloc && header->capacity.bytes == 10000);

    _free(mem);
    munmap(odd_alloc, 30);
    heap_term();
    return true;
}

int main() {
    test(test_success_alloc, "success alloc");
    test(test_free_one_block, "free one block");
    test(test_free_some_blocks, "free some blocks");
    test(test_new_region, "create new region");
    test(test_new_region_other_place, "create new region in another place");

    return 0;
}

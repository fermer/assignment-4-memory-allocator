#define _DEFAULT_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/* аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    bool extends = true;
    size_t size = region_actual_size(query + offsetof(struct block_header, contents));
    void *start_address = map_pages(addr, size, MAP_FIXED);
    if (start_address == MAP_FAILED) {
        extends = false;
        start_address = map_pages(addr, size, 0);
        if (start_address == MAP_FAILED) {
            return (struct region) REGION_INVALID;
        }
    }

    block_init(start_address, (block_size) {.bytes = size}, NULL);
    struct region region = (struct region) {.extends = extends, .addr = start_address, .size = size};
    return region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/* --- Разделение блоков (если найденный свободный блок слишком большой)--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block_splittable(block, query)) {
        struct block_header *next_block = block->next;
        void *first_block_address = block;
        void *second_block_address = query + block->contents;
        block_size first_block_size = {.bytes = size_from_capacity((block_capacity) {.bytes = query}).bytes};
        block_size second_block_size = {.bytes = block->capacity.bytes - query};
        block_init(first_block_address, first_block_size, second_block_address);
        block_init(second_block_address, second_block_size, next_block);
        return true;
    }
    return false;
}

static bool try_merge_with_next(struct block_header *block) {
    if (block->next != NULL && mergeable(block, block->next)) {
        struct block_header *block_next = block->next->next;
        block_size size = {.bytes = 2 * offsetof(struct block_header, contents) + block->capacity.bytes +
        block->next->capacity.bytes};
        block_init(block, size, block_next);
        return true;
    }
    return false;
}

/* освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header *block = (struct block_header *) HEAP_START;
    while (block != NULL) {
        size_t length = size_from_capacity(block->capacity).bytes;
        munmap(block, length);
        block = block->next;
    }
}

/* --- ... если размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_search_result bsr = {.type = BSR_CORRUPTED};
    struct block_header *cur_block = block;
    struct block_header *prev_block = NULL;
    bool is_corrupted = true;
    while (cur_block != NULL) {
        is_corrupted = false;
        while (try_merge_with_next(cur_block)) {};
        if (cur_block->capacity.bytes >= sz && cur_block->is_free) {
            bsr.type = BSR_FOUND_GOOD_BLOCK;
            bsr.block = cur_block;
            return bsr;
        }
        prev_block = cur_block;
        cur_block = cur_block->next;
    }
    if (!cur_block && !is_corrupted) {
        bsr.block = prev_block;
        bsr.type = BSR_REACHED_END_NOT_FOUND;
    }
    return bsr;
}

/* Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result bsr = find_good_or_last(block, query);
    if (bsr.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(bsr.block, query);
        bsr.block->is_free = false;
    }
    return bsr;
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    struct region region = alloc_region(last->contents + last->capacity.bytes, query);
    if (region_is_invalid(&region)) {
        return NULL;
    }
    last->next = region.addr;
    try_merge_with_next(last);
    struct block_header *block = last;
    if (!region.extends || !last->is_free) {
        block = region.addr;
    }
    return block;
}

/* Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;
    struct block_search_result bsr = try_memalloc_existing(query, heap_start);
    switch (bsr.type) {
        case BSR_FOUND_GOOD_BLOCK:
            return bsr.block;
        case BSR_CORRUPTED:
            return NULL;
        case BSR_REACHED_END_NOT_FOUND: {
            struct block_header *block = grow_heap(bsr.block, query);
            bsr = try_memalloc_existing(query, block);
        }
    }
    return bsr.block;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    try_merge_with_next(header);
}

void regions_print() {
    struct block_header *block = (struct block_header *) HEAP_START;
    while (block != NULL) {
        printf("capacity: %zu\n", block->capacity.bytes);
        printf("is free: %s\n", block->is_free ? "true" : "false");
        printf("--------------------------\n");

        block = block->next;
    }
}
